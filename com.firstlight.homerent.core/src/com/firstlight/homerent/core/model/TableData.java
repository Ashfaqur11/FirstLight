package com.firstlight.homerent.core.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class TableData {

	protected List<TableCell> rowInfo = new ArrayList<TableCell>();

	public TableData() {
	}

	public TableData(List<TableCell> tableCells) {
		this.rowInfo = tableCells;
	}

	public List<TableCell> getRowInfo() {
		return this.rowInfo;
	}

	public void validate(String data, String field, boolean mandatory) throws Exception {
		if (mandatory) {
			Objects.requireNonNull(data, "'" + field + "' field is mandatory");
		}
		if (data != null && data.length() > 1000) {
			throw new Exception(
					"Given value for '" + field + "' field is more than 1000 character limit");
		}
	}
}
