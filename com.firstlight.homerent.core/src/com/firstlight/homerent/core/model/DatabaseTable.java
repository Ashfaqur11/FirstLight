package com.firstlight.homerent.core.model;

import java.util.ArrayList;
import java.util.List;

public abstract class DatabaseTable {

	public List<TableColumn> columns = new ArrayList<TableColumn>();
	private final String tableName;

	public enum DataType {
		TEXT, INTEGER;
	}

	public DatabaseTable(String name) {
		this.tableName = name;
	}

	public List<TableColumn> getColumns() {
		return this.columns;
	}

	public String getTableName() {
		return tableName;
	}

	public static void validateContainsDefaultColumns(List<TableColumn> columns,
			List<String> defaultColumns) throws Exception {
		for (String defaultcolumn : defaultColumns) {
			boolean found = false;
			for (TableColumn col : columns) {
				if (col.getName().equals(defaultcolumn)) {
					found = true;
					break;
				}
			}
			if (!found) {
				throw new Exception(
						"Table does not contain the default column '" + defaultcolumn + "'");
			}
		}
	}

	public abstract void createTable(List<TableColumn> columns) throws Exception;

	public abstract void createDefaultTable();
	
	public abstract List<TableColumn> getDefaultColumns();

	public abstract String getPrimaryKey();

	public abstract ForeignKey getForeignKey();

}
