package com.firstlight.homerent.core.model;

public class TableCell {

	private String columnName;
	private String cellValue;

	public TableCell(String name, String value) {
		this.columnName = name;
		if (value != null && value.equalsIgnoreCase("null")) {
			value = null;
		}

		this.cellValue = value;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	public void setCellValue(String cellValue) {
		this.cellValue = cellValue;
	}

	public String getColumnName() {
		return columnName;
	}

	public String getCellValue() {
		return cellValue;
	}
}
