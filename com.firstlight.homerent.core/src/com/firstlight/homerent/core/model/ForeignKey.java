package com.firstlight.homerent.core.model;

public class ForeignKey {

	private final String columnName;
	private final String foreignTableName;
	private final String foreignColumnName;

	public ForeignKey(String columnName, DatabaseTable foreignTable) {
		this.columnName = columnName;
		this.foreignTableName = foreignTable.getTableName();
		this.foreignColumnName = foreignTable.getPrimaryKey();
	}

	public String getColumnName() {
		return columnName;
	}

	public String getForeignTableName() {
		return foreignTableName;
	}

	public String getForeignColumnName() {
		return foreignColumnName;
	}



}
