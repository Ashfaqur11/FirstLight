package com.firstlight.homerent.core.model;

import java.util.Objects;

import com.firstlight.homerent.core.model.DatabaseTable.DataType;

public class TableColumn {

	private final int order;
	private final String name;
	private final DataType type;

	public TableColumn(int order, String name, DataType type) {
		this.order = order;
		this.name = name;
		this.type = type;
	}

	public TableColumn(int order, String name, String type) {
		Objects.requireNonNull(order, "Order of Table Column cannot be null");
		Objects.requireNonNull(name, "Name of Table Column cannot be null");
		Objects.requireNonNull(type, "Type of Table Column cannot be null");
		this.order = order;
		this.name = name;
		DataType dataType = null;
		for (DataType value : DataType.values()) {
			if (value.toString().equals(type)){
				dataType = value;
				break;
			}
		}
		if (dataType == null){
			throw new IllegalArgumentException(
					"Data Type '" + type + "' of a table column is invalid");
		}
		this.type = dataType;
	}

	public String getName() {
		return name;
	}

	public DataType getType() {
		return type;
	}

	public int getOrder() {
		return order;
	}

}
