package com.firstlight.homerent.core.model;

import java.util.List;

public class TenentData extends TableData {

	private String uuid = null;
	private String tenentName = null;
	private String tenentEmail = null;
	private String tenentPhone = null;
	private String tenentHouse = null;

	public TenentData(List<TableCell> rows) throws Exception {
		super(rows);
		for (TableCell cell : rows) {
			if (cell.getColumnName().equals(TenentTable.DefaultColumns.ID.toString())) {
				this.uuid = cell.getCellValue();
			} else if (cell.getColumnName()
					.equals(TenentTable.DefaultColumns.NAME.toString())) {
				this.tenentName = cell.getCellValue();
			} else if (cell.getColumnName()
					.equals(TenentTable.DefaultColumns.EMAIL.toString())) {
				this.tenentEmail = cell.getCellValue();
			} else if (cell.getColumnName().equals(TenentTable.DefaultColumns.PHONE.toString())) {
				this.tenentPhone = cell.getCellValue();
			} else if (cell.getColumnName()
					.equals(TenentTable.DefaultColumns.HOUSE_ID.toString())) {
				this.tenentHouse = cell.getCellValue();
			}
		}
		validate(this.uuid, TenentTable.DefaultColumns.ID.toString(), true);
		validate(this.tenentName, TenentTable.DefaultColumns.NAME.toString(), false);
		validate(this.tenentEmail,
				TenentTable.DefaultColumns.EMAIL.toString(), false);
		validate(this.tenentPhone,
				TenentTable.DefaultColumns.PHONE.toString(), false);
		validate(this.tenentHouse, 
				TenentTable.DefaultColumns.HOUSE_ID.toString(), true);
	}

	public String getId() {
		return uuid;
	}

	public String getName() {
		return tenentName;
	}

	public String getEmail() {
		return tenentEmail;
	}

	public String getPhone() {
		return tenentPhone;
	}

	public String getHouseId() {
		return tenentHouse;
	}
}
