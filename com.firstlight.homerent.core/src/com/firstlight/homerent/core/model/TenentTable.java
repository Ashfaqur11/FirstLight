package com.firstlight.homerent.core.model;

import java.util.ArrayList;
import java.util.List;

public class TenentTable extends DatabaseTable {

	public static final String TABLE_NAME = "tenent";

	public enum DefaultColumns {
		ID("Identifer", "ID"),
		NAME("Name", "Name"),
		EMAIL("Email", "Email"),
		PHONE("Phone", "Phone"),
		HOUSE_ID("HouseID", "House ID");

		private final String value;
		private final String userName;

		private DefaultColumns(String value, String userName) {
			this.value = value;
			this.userName = userName;
		}

		public String getUserVisibleName() {
			return this.userName;
		}

		@Override
		public String toString() {
			return this.value;
		}

		public static List<String> getValues() {
			List<String> list = new ArrayList<String>();
			list.add(ID.toString());
			list.add(NAME.toString());
			list.add(EMAIL.toString());
			list.add(PHONE.toString());
			list.add(HOUSE_ID.toString());
			return list;
		}

		public static List<String> getVisibleFields() {
			List<String> list = new ArrayList<String>();
			list.add(NAME.toString());
			list.add(PHONE.toString());
			list.add(EMAIL.toString());
			return list;
		}

		public static List<String> getIdFields() {
			List<String> list = new ArrayList<String>();
			list.add(ID.toString());
			list.add(HOUSE_ID.toString());
			return list;
		}
	}

	public TenentTable() {
		super(TABLE_NAME);
		createDefaultTable();
	}

	public TenentTable(List<TableColumn> columns) throws Exception {
		super(TABLE_NAME);
		createTable(columns);
	}

	@Override
	public void createTable(List<TableColumn> columns) throws Exception {
		for (TableColumn col : columns) {
			this.columns.add(new TableColumn(col.getOrder(), col.getName(), col.getType()));
		}
		validateContainsDefaultColumns(this.columns, DefaultColumns.getValues());
	}

	@Override
	public List<TableColumn> getDefaultColumns() {
		List<TableColumn> list = new ArrayList<TableColumn>();
		int counter = 0;
		list.add(new TableColumn(++counter, DefaultColumns.ID.toString(), DataType.TEXT));
		list.add(new TableColumn(++counter, DefaultColumns.NAME.toString(), DataType.TEXT));
		list.add(new TableColumn(++counter, DefaultColumns.EMAIL.toString(), DataType.TEXT));
		list.add(new TableColumn(++counter, DefaultColumns.PHONE.toString(), DataType.TEXT));
		list.add(new TableColumn(++counter, DefaultColumns.HOUSE_ID.toString(), DataType.TEXT));
		return list;

	}

	@Override
	public void createDefaultTable() {
		this.columns.addAll(getDefaultColumns());
	}

	@Override
	public String getPrimaryKey() {
		return DefaultColumns.ID.toString();
	}

	@Override
	public ForeignKey getForeignKey() {
		return new ForeignKey(DefaultColumns.HOUSE_ID.toString(), new HouseTable());
	}

}
