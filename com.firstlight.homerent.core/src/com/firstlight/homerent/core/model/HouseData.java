package com.firstlight.homerent.core.model;

import java.util.List;

public class HouseData extends TableData {

	private String uuid = null;
	private String name = null;
	private String address = null;

	/**
	 * Creates House Data
	 * @param rows Data of rows of the table
	 */
	public HouseData(List<TableCell> rows) throws Exception {
		super(rows);
		for (TableCell cell : rows) {
			if (cell.getColumnName().equals(HouseTable.DefaultColumns.ID.toString())) {
				this.uuid = cell.getCellValue();
			} else if (cell.getColumnName().equals(HouseTable.DefaultColumns.NAME.toString())) {
				this.name = cell.getCellValue();
			} else if (cell.getColumnName().equals(HouseTable.DefaultColumns.ADDRESS.toString())) {
				this.address = cell.getCellValue();
			}
		}
		validate(this.uuid, HouseTable.DefaultColumns.ID.toString(), true);
		validate(this.name, HouseTable.DefaultColumns.NAME.toString(), true);
		validate(this.address,
				HouseTable.DefaultColumns.ADDRESS.toString(), true);
	}

	public String getId() {
		return uuid;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}
}
