package com.firstlight.homerent.core.model;

import java.util.ArrayList;
import java.util.List;

public class HouseTable extends DatabaseTable {

	public static final String TABLE_NAME = "houses";

	public enum DefaultColumns {
		ID("Identifer", "ID"),
		NAME("Name", "House"),
		ADDRESS("Address", "Address");

		private final String value;
		private final String userName;

		private DefaultColumns(String value, String userName) {
			this.value = value;
			this.userName = userName;
		}

		public String getUserVisibleName() {
			return this.userName;
		}

		@Override
		public String toString() {
			return this.value;
		}

		public static List<String> getValues() {
			List<String> list = new ArrayList<String>();
			list.add(ID.toString());
			list.add(NAME.toString());
			list.add(ADDRESS.toString());
			return list;
		}

		public static List<String> getVisibleFields() {
			List<String> list = new ArrayList<String>();
			list.add(NAME.toString());
			list.add(ADDRESS.toString());
			return list;
		}

		public static List<String> getIdFields() {
			List<String> list = new ArrayList<String>();
			list.add(ID.toString());
			return list;
		}
	}

	public HouseTable() {
		super(TABLE_NAME);
		createDefaultTable();
	}

	public HouseTable(List<TableColumn> columns) throws Exception {
		super(TABLE_NAME);
		createTable(columns);
	}

	@Override
	public void createTable(List<TableColumn> columns) throws Exception {
		for (TableColumn col : columns) {
			this.columns.add(new TableColumn(col.getOrder(), col.getName(), col.getType()));
		}
		validate(this.columns);
	}

	public static void validate(List<TableColumn> columns) throws Exception {
		validateContainsDefaultColumns(columns, DefaultColumns.getValues());
	}

	@Override
	public void createDefaultTable() {
		this.columns.addAll(getDefaultColumns());
	}

	@Override
	public List<TableColumn> getDefaultColumns() {
		List<TableColumn> list = new ArrayList<TableColumn>();
		int counter = 0;
		list.add(new TableColumn(counter++, DefaultColumns.ID.toString(), DataType.TEXT));
		list.add(new TableColumn(counter++, DefaultColumns.NAME.toString(), DataType.TEXT));
		list.add(new TableColumn(counter++, DefaultColumns.ADDRESS.toString(), DataType.TEXT));
		return list;

	}

	@Override
	public String getPrimaryKey() {
		return DefaultColumns.ID.toString();
	}

	@Override
	public ForeignKey getForeignKey() {
		// No foreign key
		return null;
	}

}
