package com.firstlight.homerent.core.manager;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import com.firstlight.homerent.common.logger.Logger;
import com.firstlight.homerent.common.utils.EclipseUtils;
import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.HouseTable;
import com.firstlight.homerent.core.model.TableCell;
import com.firstlight.homerent.core.model.TableColumn;
import com.firstlight.homerent.core.model.TableData;
import com.firstlight.homerent.core.model.TenentData;
import com.firstlight.homerent.core.model.TenentTable;
import com.firstlight.homerent.core.sql.DataBaseQuery;

public class DatabaseManager {

	private static final String LOGGER_PREFIX = "[" + DatabaseManager.class.getSimpleName() + "] ";

	private Connection dbConnection;
	private final DataBaseQuery dbQuery;

	private static class Singleton {
		static final DatabaseManager INSTANCE = new DatabaseManager();
	}

	public static DatabaseManager getInstance() {
		return Singleton.INSTANCE;
	}

	private DatabaseManager() {
		this.dbQuery = new DataBaseQuery();
		initializeDB();
	}

	private void initializeDB() {
		try {
			Logger.debug(LOGGER_PREFIX + "Initializing Database");
			Class.forName("org.sqlite.JDBC");
			Logger.debug(LOGGER_PREFIX + "Creating connection to database");
			String dbPath = EclipseUtils.getWorkspaceRoot().toOSString() + File.separator
					+ "homerent.db";
			this.dbConnection = DriverManager
					.getConnection("jdbc:sqlite:" + dbPath);
			createTables();
		} catch (Exception e) {
			Logger.error(LOGGER_PREFIX + " Unable to initialze database", e);
		}
	}

	public boolean tableExists(String tableName) throws Exception {
		ResultSet result = this.dbQuery.tableExists(this.dbConnection, tableName);
		return (result.next());
	}

	private void createTables() throws Exception {
		if (!tableExists(HouseTable.TABLE_NAME)) {
			this.dbQuery.createTable(this.dbConnection, new HouseTable());
		}

		if (!tableExists(TenentTable.TABLE_NAME)) {
			this.dbQuery.createTable(this.dbConnection, new TenentTable());
		}

		if (this.dbQuery.isTableEmpty(this.dbConnection, HouseTable.TABLE_NAME)) {
			Logger.debug(LOGGER_PREFIX + "Table is empty");
			List<TableData> houseDataList = createDummyDataForHouseTable();
			this.dbQuery.addMutipleDataToTable(this.dbConnection, HouseTable.TABLE_NAME,
					houseDataList);
		}
		ResultSet result = this.dbQuery.getAllResults(this.dbConnection, HouseTable.TABLE_NAME);
		while (result.next()) {
			Logger.debug(LOGGER_PREFIX + result.getString(1) + " "
					+ result.getString(2) + " "
					+ result.getString(3));
		}

		result = this.dbQuery.getAllColumns(this.dbConnection, HouseTable.TABLE_NAME);
		while (result.next()) {
			Logger.debug(LOGGER_PREFIX + result.getInt(1) + " " + result.getString(2) + " "
					+ result.getString(3));
		}

		if (this.dbQuery.isTableEmpty(this.dbConnection, TenentTable.TABLE_NAME)) {
			Logger.debug(LOGGER_PREFIX + "Tenent Table is empty");
			List<TableData> tenentDataList = createDummyDataForTenentTable();
			this.dbQuery.addMutipleDataToTable(this.dbConnection, TenentTable.TABLE_NAME,
					tenentDataList);
		}

		result = this.dbQuery.getAllResults(this.dbConnection, TenentTable.TABLE_NAME);
		while (result.next()) {
			Logger.debug(LOGGER_PREFIX + result.getString(1) + " " + result.getString(2) + " "
					+ result.getString(3));
		}
	}

	public void deleteFromTable(HouseData data) throws SQLException {
		String value = data.getId();
		this.dbQuery.deleteFromTable(this.dbConnection, HouseTable.TABLE_NAME,
				HouseTable.DefaultColumns.ID.toString(), value);
		this.dbQuery.deleteFromTable(this.dbConnection, TenentTable.TABLE_NAME,
				TenentTable.DefaultColumns.HOUSE_ID.toString(), value);

	}

	public void addHouseData(HouseData data) throws Exception {
		this.dbQuery.addDataToTable(this.dbConnection, HouseTable.TABLE_NAME, data.getRowInfo());
	}

	public void addTenentData(TenentData data) throws Exception {
		this.dbQuery.addDataToTable(this.dbConnection, TenentTable.TABLE_NAME, data.getRowInfo());
	}

	public void updateHouseData(HouseData data) throws Exception {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.addAll(data.getRowInfo());
		Iterator<TableCell> iter = cells.iterator();
		while (iter.hasNext()) {
			TableCell cell = iter.next();
			if (HouseTable.DefaultColumns.getIdFields().contains(cell.getColumnName())) {
				iter.remove();
			}
		}
		this.dbQuery.updateTableData(this.dbConnection, HouseTable.TABLE_NAME,
				HouseTable.DefaultColumns.ID.toString(), data.getId(), cells);
	}

	public void updateTenentData(TenentData data) throws Exception {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.addAll(data.getRowInfo());
		Iterator<TableCell> iter = cells.iterator();
		while (iter.hasNext()) {
			TableCell cell = iter.next();
			if (TenentTable.DefaultColumns.getIdFields().contains(cell.getColumnName())) {
				iter.remove();
			}
		}
		this.dbQuery.updateTableData(this.dbConnection, TenentTable.TABLE_NAME,
				TenentTable.DefaultColumns.ID.toString(), data.getId(), cells);
	}

	public List<TenentData> getTenentTable() throws Exception {
		List<TenentData> list = new ArrayList<TenentData>();
		List<TableData> rows = getTableData(TenentTable.TABLE_NAME);
		for (TableData row : rows) {
			list.add(new TenentData(row.getRowInfo()));
		}
		return list;
	}

	public List<HouseData> getHouseTable() {
		List<HouseData> list = new ArrayList<HouseData>();
		try {
			List<TableData> rows = getTableData(HouseTable.TABLE_NAME);
			for (TableData row : rows) {
				list.add(new HouseData(row.getRowInfo()));
			}
		} catch (Exception e) {
			Logger.error(LOGGER_PREFIX + " Error occured retrieving info from table '"
					+ HouseTable.TABLE_NAME + "' Details: " + e.getMessage(), e);
		}
		return list;
	}

	public TenentData getTenentData(String houseId) {
		TenentData data = null;
		try {
			List<TableCell> tableCells = findTableData(TenentTable.TABLE_NAME,
					TenentTable.DefaultColumns.HOUSE_ID.toString(), houseId);
			if (!tableCells.isEmpty()) {
				data = new TenentData(tableCells);
			}
		} catch (Exception e) {
			Logger.error(LOGGER_PREFIX + " Error occured retrieving info from table '"
					+ TenentTable.TABLE_NAME + "' Details: " + e.getMessage());
		}
		return data;
	}

	public List<TableCell> findTableData(String tableName, String columnName, String value)
			throws SQLException {
		List<TableCell> cells = new ArrayList<TableCell>();

		ResultSet result = this.dbQuery.findTableData(this.dbConnection, tableName, columnName,
				value);
		ResultSetMetaData metadata = result.getMetaData();
		int columns = metadata.getColumnCount();
		while (result.next()) {
			for (int i = 1; i <= columns; i++) {
				cells.add(new TableCell(metadata.getColumnName(i), result.getString(i)));
			}
		}

		return cells;
	}


	public List<TableData> getTableData(String tableName) throws SQLException {
		List<TableData> list = new ArrayList<TableData>();
		ResultSet result = this.dbQuery.getAllResults(this.dbConnection, tableName);
		ResultSetMetaData metadata = result.getMetaData();
		int columns = metadata.getColumnCount();
		while (result.next()) {
			List<TableCell> cells = new ArrayList<TableCell>();
			for (int i = 1; i <= columns; i++) {
				cells.add(new TableCell(metadata.getColumnName(i), result.getString(i)));
			}
			list.add(new TableData(cells));
		}
		return list;
	}

	public List<TableColumn> getColumns(String tableName) throws Exception {
		ResultSet result = this.dbQuery.getAllColumns(this.dbConnection, HouseTable.TABLE_NAME);
		List<TableColumn> columns = new ArrayList<TableColumn>();
		while (result.next()) {
			columns.add(
					new TableColumn(result.getInt(1), result.getString(1), result.getString(2)));
		}
		return columns;
	}

	private String house1 = UUID.randomUUID().toString();
	private String house2 = UUID.randomUUID().toString();

	/**
	 * Creates dummy data for houses
	 * @return Dummy table data for house
	 */
	private List<TableData> createDummyDataForHouseTable() throws Exception {
		List<TableData> list = new ArrayList<TableData>();
		list.add(new HouseData(createHouse(house1, "Flat E", "Miami")));
		list.add(new HouseData(createHouse(house2, "GroundFloor", "Miami")));
		return list;
	}

	private List<TableCell> createHouse(String id, String name, String address) {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.add(new TableCell(HouseTable.DefaultColumns.ID.toString(), id));
		cells.add(new TableCell(HouseTable.DefaultColumns.NAME.toString(), name));
		cells.add(new TableCell(HouseTable.DefaultColumns.ADDRESS.toString(), address));
		return cells;
	}

	/**
	 * Creates a dummy set of data for tenents table
	 * @return Dummy table data for tenents
	 */
	private List<TableData> createDummyDataForTenentTable() throws Exception {
		List<TableData> list = new ArrayList<TableData>();
		list.add(new TenentData(
				createTenent("John Smith", "john@smith", "884646", house1)));
		list.add(new TenentData(
				createTenent("Marie Gellar", "marie@gellar", "3354543646", house2)));
		return list;
	}

	private List<TableCell> createTenent(String name, String email,
			String phone, String house) {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.add(new TableCell(TenentTable.DefaultColumns.ID.toString(),
				UUID.randomUUID().toString()));
		cells.add(new TableCell(TenentTable.DefaultColumns.NAME.toString(), name));
		cells.add(new TableCell(TenentTable.DefaultColumns.EMAIL.toString(), email));
		cells.add(new TableCell(TenentTable.DefaultColumns.PHONE.toString(), phone));
		cells.add(new TableCell(TenentTable.DefaultColumns.HOUSE_ID.toString(), house));
		return cells;
	}

	public void closeDatabase() {
		if (this.dbConnection != null) {
			try {
				this.dbConnection.close();
			} catch (SQLException e) {
				Logger.error(LOGGER_PREFIX + " Error occured closing Database connection. Details: "
						+ e.getMessage(), e);
			}
		}
	}




}
