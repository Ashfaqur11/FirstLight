package com.firstlight.homerent.core.sql;

import java.util.List;

import com.firstlight.homerent.core.model.ForeignKey;
import com.firstlight.homerent.core.model.TableCell;
import com.firstlight.homerent.core.model.TableColumn;
import com.firstlight.homerent.core.model.TableData;

/**
 * A utility class to build SQL command strings
 */
public class SqlBuilder {

	/**
	 * Create a SQL command to find a table in Database.
	 * @param tableName Name of the table to find
	 * @return SQL command string
	 */
	public static String findTable(String tableName) {
		String sql = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableName
				+ "'";
		return sql;
	}


	/**
	 * Create a SQL command to create Table in Database.
	 * @param tableName Name of the Table
	 * @param columns Columns of the Table
	 * @param primaryKey Primary Key of the Table
	 * @param foreignKey Foreign Key (Optional)
	 * @return SQL command string
	 */
	public static String createTable(String tableName, List<TableColumn> columns,
			String primaryKey, ForeignKey foreignKey) {

		boolean keyFound = false;
		String sql = "CREATE TABLE IF NOT EXISTS " + tableName + " (";

		for (int i = 0; i < columns.size(); i++) {
			TableColumn column = columns.get(i);
			String name = column.getName();
			String type = column.getType().toString();
			sql = sql + name + " " + type;
			if (!keyFound && primaryKey.equals(name)) {
				sql = sql + " PRIMARY KEY";
				keyFound = true;
			}
			if (i != (columns.size() - 1)) {
				sql = sql + ", ";
			}
		}
		if (foreignKey != null) {
			sql = sql + ", FOREIGN KEY(" + foreignKey.getColumnName() + ") REFERENCES "
					+ foreignKey.getForeignTableName() + "(" + foreignKey.getForeignColumnName()
					+ ")";
		}
		sql = sql + ");";
		return sql;
	}


	public static String findData(String tableName, String columnName, String value) {
		return "SELECT * FROM " + tableName + " WHERE " + columnName + "='" + value + "'";
	}

	public static String getColumns(String tableName) {
		return "PRAGMA table_info(" + tableName + ");";
	}

	public static String allresults(String tableName) {
		String sql = "SELECT * FROM " + tableName + ";";
		return sql;
	}

	/**
	 * Create sql command to insert multiple data to Table
	 * @param table Name of the table
	 * @param rows Data rows of the table
	 * @return SQL command string
	 */
	public static String insertMutipleDataToTable(String table, List<TableData> rows) {
		String sql = "";
		for (int i = 0; i < rows.size(); i++) {
			sql = sql + insertDataToTable(table, rows.get(i).getRowInfo());
		}
		return sql;
	}

	public static String deleteFromTable(String table, String id, String idValue) {
		String sql = "DELETE FROM " + table + " WHERE " + id + " = '" + idValue + "'";
		return sql;
	}

	public static String updateTable(String tableName, String id, String idValue,
			List<TableCell> cells) {
		String sql = "UPDATE " + tableName + " SET ";
		for (int i = 0; i < cells.size(); i++) {
			sql = sql + cells.get(i).getColumnName() + " = '" + cells.get(i).getCellValue() + "'";
			if (i != cells.size() -1){
				sql = sql + ", ";
			}
		}
		sql = sql + " WHERE " + id + " = '" + idValue + "'";
		return sql;

	}

	/**
	 * Create SQL command to insert data to Table
	 * @param tableName Name of the Table
	 * @param cells List of cells containing data of a table
	 * row
	 * @return SQL command string
	 */
	public static String insertDataToTable(String tableName, List<TableCell> cells) {
		String sql = "INSERT INTO " + tableName;
		String columns = "(";
		String values = "(";
		for (int i = 0; i < cells.size(); i++) {
			TableCell cell = cells.get(i);
			columns = columns + cell.getColumnName();
			values = values + "'" + cell.getCellValue() + "'";
			if (i != (cells.size() - 1)) {
				columns = columns + ", ";
				values = values + ", ";
			}
		}
		columns = columns + ")";
		values = values + ");";
		
		sql = sql + " " + columns + " VALUES " + values;
		return sql;
	}
}
