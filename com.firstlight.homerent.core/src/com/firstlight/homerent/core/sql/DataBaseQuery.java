package com.firstlight.homerent.core.sql;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.firstlight.homerent.common.logger.Logger;
import com.firstlight.homerent.core.model.DatabaseTable;
import com.firstlight.homerent.core.model.TableCell;
import com.firstlight.homerent.core.model.TableData;

public class DataBaseQuery {

	private final String LOGGER_PREFIX = "[" + DataBaseQuery.class.getSimpleName() + "] ";

	public ResultSet tableExists(Connection conn, String name) throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.findTable(name);
		Logger.debug(LOGGER_PREFIX + sql);
		ResultSet result = state.executeQuery(sql);
		return result;
	}

	public void createTable(Connection conn, DatabaseTable table)
			throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.createTable(table.getTableName(), table.getColumns(),
				table.getPrimaryKey(), table.getForeignKey());
		Logger.debug(LOGGER_PREFIX + sql);
		state.execute(sql);
	}

	public ResultSet getAllColumns(Connection conn, String tableName) throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.getColumns(tableName);
		Logger.debug(LOGGER_PREFIX + sql);
		ResultSet result = state.executeQuery(sql);
		return result;
	}

	public ResultSet getAllResults(Connection conn, String tableName) throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.allresults(tableName);
		Logger.debug(LOGGER_PREFIX + sql);
		ResultSet result = state.executeQuery(sql);
		return result;
	}

	public ResultSet findTableData(Connection conn, String tableName, String columnName,
			String value) throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.findData(tableName, columnName, value);
		Logger.debug(LOGGER_PREFIX + sql);
		ResultSet result = state.executeQuery(sql);
		return result;
	}

	public void updateTableData(Connection conn, String tableName, String id, String idValue,
			List<TableCell> cells) throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.updateTable(tableName, id, idValue, cells);
		Logger.debug(LOGGER_PREFIX + sql);
		state.execute(sql);
	}

	public void deleteFromTable(Connection conn, String tableName, String id, String idValue)
			throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.deleteFromTable(tableName, id, idValue);
		Logger.debug(LOGGER_PREFIX + sql);
		state.execute(sql);
	}

	public boolean isTableEmpty(Connection conn, String tableName) throws SQLException {
		ResultSet result = getAllResults(conn, tableName);
		return !result.next();
	}

	public void addMutipleDataToTable(Connection conn, String tableName,
			List<TableData> tableData) throws SQLException {
		for (TableData data : tableData) {
			Statement state = conn.createStatement();
			String sql = SqlBuilder.insertDataToTable(tableName, data.getRowInfo());
			Logger.debug(LOGGER_PREFIX + sql);
			state.execute(sql);
		}
	}

	public void addDataToTable(Connection conn, String tableName, List<TableCell> tablecells)
			throws SQLException {
		Statement state = conn.createStatement();
		String sql = SqlBuilder.insertDataToTable(tableName, tablecells);
		Logger.debug(LOGGER_PREFIX + sql);
		state.execute(sql);
	}
}
