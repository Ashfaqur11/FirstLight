= README
:doctype: book

== Introduction

The purpose of the first light project is to create a Housing Rent software for personal Home Owners with small to medium sized properties.

image::HomeRent.jpg[width=640,height=480]
