package com.firstlight.homerent.core.ui.views;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.part.ViewPart;

public class RentView extends ViewPart {
	
	public static final String ID = "com.firstlight.homerent.core.ui.views.rentview";
	
	public static final String VIEW_NAME = "Rent";

	private Composite composite;

	public RentView() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void createPartControl(Composite parent) {
		this.composite = new Composite(parent, SWT.NONE);
		this.composite.setLayout(new FillLayout());
		this.composite.setBackground(new Color(parent.getDisplay(), 200, 110, 250));
		

	}

	@Override
	public void setFocus() {
		this.composite.setFocus();
	}

}
