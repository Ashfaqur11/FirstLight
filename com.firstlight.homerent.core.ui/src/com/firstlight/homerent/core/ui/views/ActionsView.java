package com.firstlight.homerent.core.ui.views;


import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import com.firstlight.homerent.common.logger.Logger;
import com.firstlight.homerent.common.utils.EclipseUtils;
import com.firstlight.homerent.common.utils.ImageUtil;
import com.firstlight.homerent.core.ui.Activator;
import com.firstlight.homerent.core.ui.view.house.HousesView;

public class ActionsView extends ViewPart {
	
	public static final String ID = "com.firstlight.homerent.core.ui.views.actionsview";
	
	private Composite composite;

	@Override
	public void createPartControl(Composite parent) {
		this.composite = new Composite(parent, SWT.BORDER);
		this.composite.setLayout(new GridLayout());
		
		Button housesButton = new Button(composite, SWT.NONE);
		Image house = ImageUtil.loadImage(Activator.PLUGIN_ID, "house_one.png");
		housesButton.setImage(house);
		housesButton.addSelectionListener(getHouseButtonListener());
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(housesButton);
		
		Label housesLabel = new Label(composite, SWT.NONE);
		housesLabel.setText("Houses");
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(housesLabel);

		Button tenentsButton = new Button(composite, SWT.NONE);
		Image tenents = ImageUtil.loadImage(Activator.PLUGIN_ID, "money_dollar.png");
		tenentsButton.setImage(tenents);
		tenentsButton.addSelectionListener(getTenentsButtonListener());
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(tenentsButton);

		Label tenentsLabel = new Label(composite, SWT.NONE);
		tenentsLabel.setText("Rent");
		GridDataFactory.fillDefaults().grab(true, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(tenentsLabel);

	}

	private SelectionListener getHouseButtonListener() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				try {
					EclipseUtils.openView(HousesView.ID);
				} catch (PartInitException e) {
					Logger.error("Unable to open " + HousesView.VIEW_NAME + " view.", e);
				}
			}
		};
	}

	private SelectionListener getTenentsButtonListener() {
		return new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				try {
					EclipseUtils.openView(RentView.ID);
				} catch (PartInitException e) {
					Logger.error("Unable to open " + RentView.VIEW_NAME + " view.", e);
				}
			}
		};
	}


	@Override
	public void setFocus() {
		this.composite.setFocus();
	}

}
