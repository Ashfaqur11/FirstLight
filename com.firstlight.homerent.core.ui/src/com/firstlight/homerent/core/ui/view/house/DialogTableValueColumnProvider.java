package com.firstlight.homerent.core.ui.view.house;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.firstlight.homerent.core.model.TableCell;

public class DialogTableValueColumnProvider extends ColumnLabelProvider {

	private final Image editImage;

	public DialogTableValueColumnProvider(Image edit) {
		this.editImage = edit;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String value = cell.getCellValue();
			if (value != null) {
				return value;
			}
		}
		return "";
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String value = cell.getCellValue();
			if (value == null || value.isEmpty()) {
				return this.editImage;
			}
		}
		return null;
	}

}
