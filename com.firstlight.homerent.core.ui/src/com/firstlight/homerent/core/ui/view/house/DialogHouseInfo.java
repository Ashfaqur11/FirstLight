package com.firstlight.homerent.core.ui.view.house;


import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.firstlight.homerent.common.logger.Logger;
import com.firstlight.homerent.common.utils.ImageUtil;
import com.firstlight.homerent.core.manager.DatabaseManager;
import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.TenentData;
import com.firstlight.homerent.core.ui.Activator;

public class DialogHouseInfo extends TitleAreaDialog {

	private final String title;
	private final String titleInfo;

	private final HouseInfo houseInfo;

	private final Image addImage;
	private final Image editImage;

	private Button okButton;

	private final int TABLE_WIDTH = 560;
	private final int TABLE_HEIGHT = 150;

	private final int PROPERTY_COL_WIDTH = 150;
	private final int VALUE_COL_WIDTH = 400;

	private final DialogDataModel model;

	protected DialogHouseInfo(Shell parentShell, String title, String titleInfo, HouseInfo info) {
		super(parentShell);
		this.title = title;
		this.titleInfo = titleInfo;
		this.houseInfo = info;
		this.model = new DialogDataModel(this.houseInfo);
		this.addImage = ImageUtil.loadImage(Activator.PLUGIN_ID, "plus_light_blue.png");
		this.editImage = ImageUtil.loadImage(Activator.PLUGIN_ID, "pencil.png");
		setDefaultImage((this.houseInfo == null) ? this.addImage : this.editImage);
		setShellStyle(getShellStyle() | SWT.RESIZE);
	}

	@Override
	protected void okPressed() {
		super.okPressed();
		try {
			HouseInfo newInfo = model.getHouseInfo();
			HouseData houseData = newInfo.getHouseData();
			TenentData tenentData = newInfo.getTenentData();
			DatabaseManager manager = DatabaseManager.getInstance();
			if (this.houseInfo == null) {
				manager.addHouseData(houseData);
				manager.addTenentData(tenentData);
			} else {
				manager.updateHouseData(houseData);
				manager.updateTenentData(tenentData);
			}
		} catch (Exception e) {
			Logger.error("Unable to save house information. Details: " + e.getMessage(), e);
		}
	}

	public void cleanup() {
		this.addImage.dispose();
		this.editImage.dispose();
	}

	@Override
	public void create() {
		super.create();
		setTitle(this.titleInfo);
		this.okButton = getButton(IDialogConstants.OK_ID);
		this.okButton.setEnabled(false);
		setMessage((this.houseInfo == null ? "Add" : "Edit") + " house information",
				IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		container.setLayout(new GridLayout());

		createHouseInfoTable(container);
		createTenentInfoTable(container);

		return container;
	}

	private void createHouseInfoTable(Composite parent) {
		Group container = new Group(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).hint(TABLE_WIDTH, TABLE_HEIGHT)
				.applyTo(container);
		container.setLayout(new FillLayout());

		container.setText("House Information");

		TableViewer tableViewer = createTable(container, true);

	}

	private TableViewer createTable(Composite composite, boolean houseTable) {
		TableViewer tableViewer = new TableViewer(composite,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		final Table table = tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());

		TableViewerColumn propertyColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		propertyColumn.getColumn().setWidth(PROPERTY_COL_WIDTH);
		propertyColumn.getColumn().setText("Property");
		propertyColumn.getColumn().setMoveable(true);

		TableViewerColumn valueColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		valueColumn.getColumn().setWidth(VALUE_COL_WIDTH);
		valueColumn.getColumn().setText("Value");
		valueColumn.getColumn().setMoveable(true);

		propertyColumn.setLabelProvider(
				new DialogTablePropertyColumnProvider(this.addImage, this.editImage));
		valueColumn.setLabelProvider(new DialogTableValueColumnProvider(this.editImage));
		valueColumn.setEditingSupport(
				new DialogCellEditingSupport(valueColumn.getViewer(), tableViewer, this,
						this.model));

		if (houseTable) {
			tableViewer.setInput(this.model.getHouseInfoCells());
		} else {
			tableViewer.setInput(this.model.getTenentInfoCells());
		}
		return tableViewer;
	}

	public void validate() {
		String msg = null;
		try {
			new HouseData(this.model.getHouseAllCells());
		} catch (Exception e) {
			msg = e.getMessage() + " for a house";
		}

		if (msg == null) {
			try {
				new TenentData(this.model.getTenentAllCells());
			} catch (Exception e) {
				msg = e.getMessage() + " for a tenent";
			}
		}

		if (msg != null) {
			setMessage(msg, IMessageProvider.ERROR);
		} else {
			setMessage("Press ok to save the information", IMessageProvider.INFORMATION);
		}
		this.okButton.setEnabled(msg == null);
	}


	private void createTenentInfoTable(Composite parent) {

		Group container = new Group(parent, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).hint(TABLE_WIDTH, TABLE_HEIGHT)
				.applyTo(container);
		container.setLayout(new FillLayout());

		container.setText("Tenent Information");
		TableViewer tableViewer = createTable(container, false);

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());

	}

	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText(title);
	}
}

