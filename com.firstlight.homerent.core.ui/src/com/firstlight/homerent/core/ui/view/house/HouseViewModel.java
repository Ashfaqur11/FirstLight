package com.firstlight.homerent.core.ui.view.house;

import java.util.ArrayList;
import java.util.List;

import com.firstlight.homerent.core.manager.DatabaseManager;
import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.HouseTable;
import com.firstlight.homerent.core.model.TenentData;

public class HouseViewModel {

	private List<HouseInfo> houseInfoList = new ArrayList<HouseInfo>();
	private DatabaseManager managerDB;

	private String[] columns = new String[] {
			HouseTable.DefaultColumns.NAME.getUserVisibleName(),
			HouseTable.DefaultColumns.ADDRESS.getUserVisibleName() };

	public HouseViewModel() {
		this.managerDB = DatabaseManager.getInstance();
		createHouseInfoList();
	}

	private void createHouseInfoList() {
		List<HouseData> houses = this.managerDB.getHouseTable();
		this.houseInfoList = new ArrayList<HouseInfo>();
		for (HouseData house : houses) {
			TenentData tenent = this.managerDB.getTenentData(house.getId());
			this.houseInfoList.add(new HouseInfo(house, tenent));
		}
	}

	public List<HouseInfo> getAllHouseInfo() {
		return this.houseInfoList;
	}
}
