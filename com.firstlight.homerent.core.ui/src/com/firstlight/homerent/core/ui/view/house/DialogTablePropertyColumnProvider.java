package com.firstlight.homerent.core.ui.view.house;

import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.firstlight.homerent.core.model.TableCell;

public class DialogTablePropertyColumnProvider extends ColumnLabelProvider {

	private final Image editImage;
	private final Image addImage;

	public DialogTablePropertyColumnProvider(Image add, Image edit) {
		this.addImage = add;
		this.editImage = edit;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String columnName = cell.getColumnName();
			if (columnName != null) {
				return columnName;
			}
		}
		return "";
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String columnName = cell.getColumnName();
			if (columnName == null) {
				return this.addImage;
			} else if (columnName.isEmpty()) {
				return this.editImage;
			}
		}
		return null;
	}

}
