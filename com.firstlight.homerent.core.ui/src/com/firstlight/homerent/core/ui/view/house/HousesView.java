package com.firstlight.homerent.core.ui.view.house;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.part.ViewPart;

import com.firstlight.homerent.common.logger.Logger;
import com.firstlight.homerent.common.utils.ImageUtil;
import com.firstlight.homerent.core.manager.DatabaseManager;
import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.TenentData;
import com.firstlight.homerent.core.ui.Activator;

public class HousesView extends ViewPart {
	
	public static final String ID = "com.firstlight.homerent.core.ui.views.housesview";
	
	public static final String VIEW_NAME = "Houses";

	private Composite composite;
	private TableViewer tableViewer;

	private List<Image> imageList = new ArrayList<Image>();

	private Button deleteBtn;
	private Button editBtn;

	private HouseViewModel model;

	public HousesView() {
		this.model = new HouseViewModel();
	}

	@Override
	public void createPartControl(Composite parent) {
		this.composite = new Composite(parent, SWT.NONE);
		this.composite.setLayout(new GridLayout(1, false));

		createToolbar(parent);
		createTable(parent);

	}

	private void createToolbar(Composite parent) {

		int noOfButtons = 3;

		Composite toolbar = new Composite(this.composite, SWT.BORDER);
		GridDataFactory.fillDefaults().grab(true, false).applyTo(toolbar);
		toolbar.setLayout(new GridLayout(noOfButtons, true));
		toolbar.setBackground(parent.getBackground());

		Image addHouseImg = ImageUtil.loadImage(Activator.PLUGIN_ID, "house_go.png");
		this.imageList.add(addHouseImg);
		Button addHouseBtn = new Button(toolbar, SWT.PUSH);
		addHouseBtn.setImage(addHouseImg);
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(addHouseBtn);
		addHouseBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openDialog(parent.getShell(), "Add New House",
						"Enter information about the new House", null);
			}
		});

		Image editImg = ImageUtil.loadImage(Activator.PLUGIN_ID, "user_edit.png");
		this.imageList.add(editImg);
		this.editBtn = new Button(toolbar, SWT.PUSH);
		this.editBtn.setImage(editImg);
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(this.editBtn);
		this.editBtn.setEnabled(false);
		this.editBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openEditDialog();
			}
		});
		
		Image deleteImg = ImageUtil.loadImage(Activator.PLUGIN_ID, "cross.png");
		this.deleteBtn = new Button(toolbar, SWT.PUSH);
		this.deleteBtn.setImage(deleteImg);
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(this.deleteBtn);
		this.deleteBtn.setEnabled(false);
		this.deleteBtn.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				openDeleteDialog();
			}
		});

		Label addHouseLabel = new Label(toolbar, SWT.NONE);
		addHouseLabel.setText("Add House");
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(addHouseLabel);

		Label editLabel = new Label(toolbar, SWT.NONE);
		editLabel.setText("Edit");
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(editLabel);

		Label deleteLabel = new Label(toolbar, SWT.NONE);
		deleteLabel.setText("Delete");
		GridDataFactory.fillDefaults().grab(false, false).align(SWT.CENTER, SWT.CENTER)
				.applyTo(deleteLabel);
	}

	private void createTable(Composite parent) {
		Composite viewArea = new Composite(this.composite, SWT.NONE);
		GridDataFactory.fillDefaults().grab(true, true).applyTo(viewArea);
		viewArea.setLayout(new FillLayout());

		this.tableViewer = new TableViewer(viewArea,
				SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);

		final Table table = this.tableViewer.getTable();
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		this.tableViewer.setContentProvider(ArrayContentProvider.getInstance());

		createColumns(this.tableViewer);

		List<HouseInfo> list = new ArrayList<HouseInfo>();
		list.add(new HouseInfo(null, null));
		list.add(new HouseInfo(null, null));

		this.tableViewer.setInput(this.model.getAllHouseInfo());

		this.tableViewer.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
				editBtn.setEnabled(!selection.isEmpty());
				deleteBtn.setEnabled(!selection.isEmpty());
			}
		});

		this.tableViewer.addDoubleClickListener(new IDoubleClickListener() {
			@Override
			public void doubleClick(DoubleClickEvent event) {
				openEditDialog();
			}
		});

	}

	private void openDeleteDialog() {
		IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
		Object[] selections = selection.toArray();
		if (!selection.isEmpty()) {
			if (selections[0] instanceof HouseInfo) {
				HouseInfo info = (HouseInfo) selections[0];
				String houseName = info.getHouseData().getName();
				String shellTitle = "Confirm delete house '" + houseName + "'";
				String message = "Are you sure you want to delete all information related to this house including tenent and rent?";
				boolean result = MessageDialog.openQuestion(this.tableViewer.getTable().getShell(),
						shellTitle, message);
				if (result) {
					try {
						DatabaseManager.getInstance().deleteFromTable(info.getHouseData());
						this.model = new HouseViewModel();
						this.tableViewer.setInput(this.model.getAllHouseInfo());
					} catch (SQLException e) {
						Logger.error("Unable to delete house data from database. Details: "
								+ e.getMessage(), e);
					}
				}
			}
		}
	}

	private void openEditDialog() {
		IStructuredSelection selection = (IStructuredSelection) tableViewer.getSelection();
		Object[] selections = selection.toArray();
		if (!selection.isEmpty()) {
			if (selections[0] instanceof HouseInfo) {
				openDialog(this.tableViewer.getTable().getShell(), "Edit House Information",
						"Edit information of selected House", (HouseInfo) selections[0]);
			}
		}
	}

	private void openDialog(Shell shell, String title, String titleInfo, HouseInfo info) {
		DialogHouseInfo dialog = new DialogHouseInfo(shell, title, titleInfo, info);
		int result = dialog.open();
		if (result == DialogHouseInfo.OK) {
			this.model = new HouseViewModel();
			this.tableViewer.setInput(this.model.getAllHouseInfo());
		}
		dialog.cleanup();
	}

	private void createColumns(TableViewer table) {

		TableViewerColumn house = createColumn(table, "House", 200);
		house.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				String value = "";
				if (element instanceof HouseInfo) {
					HouseInfo info = (HouseInfo) element;
					HouseData data = info.getHouseData();
					if (data != null) {
						value = data.getName();
					}
				}
				return value;
			}
		});

		TableViewerColumn tenent = createColumn(table, "Tenent", 300);
		tenent.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				String value = "";
				if (element instanceof HouseInfo) {
					HouseInfo info = (HouseInfo) element;
					TenentData tenent = info.getTenentData();
					if (tenent != null) {
						value = tenent.getName();
					}
				}
				return value;
			}
		});

		TableViewerColumn address = createColumn(table, "Address", 400);
		address.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				String value = "";
				if (element instanceof HouseInfo) {
					HouseInfo info = (HouseInfo) element;
					HouseData data = info.getHouseData();
					if (data != null) {
						value = data.getAddress();
					}
				}
				return value;
			}
		});

	}

	private TableViewerColumn createColumn(TableViewer table, String name, int width) {
		TableViewerColumn col = new TableViewerColumn(table, SWT.NONE);
		col.getColumn().setWidth(width);
		col.getColumn().setText(name);
		col.getColumn().setMoveable(true);
		return col;
	}

	@Override
	public void dispose() {
		for (Image image : this.imageList) {
			if (!image.isDisposed()) {
				image.dispose();
			}
		}
		super.dispose();
	}

	@Override
	public void setFocus() {
		this.composite.setFocus();
	}

}
