package com.firstlight.homerent.core.ui.view.house;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.HouseTable;
import com.firstlight.homerent.core.model.TableCell;
import com.firstlight.homerent.core.model.TenentData;
import com.firstlight.homerent.core.model.TenentTable;

public class DialogDataModel {

	private List<TableCell> houseDataIdCells = new ArrayList<TableCell>();
	private List<TableCell> houseDataInfoCells = new ArrayList<TableCell>();

	private List<TableCell> tenentDataIdCells = new ArrayList<TableCell>();
	private List<TableCell> tenentDataInfoCells = new ArrayList<TableCell>();

	public DialogDataModel(HouseInfo houseInfo) {
		if (houseInfo != null) {
			HouseData houseData = houseInfo.getHouseData();
			TenentData tenentData = houseInfo.getTenentData();
			if (houseData != null) {
				for (TableCell cell : houseData.getRowInfo()) {
					if (HouseTable.DefaultColumns.getIdFields().contains(cell.getColumnName())) {
						this.houseDataIdCells.add(cell);
					} else {
						this.houseDataInfoCells.add(cell);
					}
				}
			}
			if (tenentData != null) {
				for (TableCell cell : tenentData.getRowInfo()) {
					if (TenentTable.DefaultColumns.getIdFields().contains(cell.getColumnName())) {
						this.tenentDataIdCells.add(cell);
					} else {
						this.tenentDataInfoCells.add(cell);
					}
				}
			}
		}
		if (this.houseDataIdCells.isEmpty()) {
			this.houseDataIdCells.add(new TableCell(HouseTable.DefaultColumns.ID.toString(),
					UUID.randomUUID().toString()));
		}
		if (this.tenentDataIdCells.isEmpty()) {
			this.tenentDataIdCells.add(new TableCell(TenentTable.DefaultColumns.ID.toString(),
					UUID.randomUUID().toString()));
			this.tenentDataIdCells.add(new TableCell(TenentTable.DefaultColumns.HOUSE_ID.toString(),
					this.houseDataIdCells.get(0).getCellValue()));
		}

		if (this.houseDataInfoCells.isEmpty()) {
			this.houseDataInfoCells
					.add(new TableCell(HouseTable.DefaultColumns.NAME.toString(), null));
			this.houseDataInfoCells
					.add(new TableCell(HouseTable.DefaultColumns.ADDRESS.toString(), null));
		}

		if (this.tenentDataInfoCells.isEmpty()) {
			this.tenentDataInfoCells
					.add(new TableCell(TenentTable.DefaultColumns.NAME.toString(), null));
			this.tenentDataInfoCells
					.add(new TableCell(TenentTable.DefaultColumns.PHONE.toString(), null));
			this.tenentDataInfoCells
					.add(new TableCell(TenentTable.DefaultColumns.EMAIL.toString(), null));
		}
	}

	public List<TableCell> getHouseInfoCells() {
		return this.houseDataInfoCells;
	}

	public List<TableCell> getHouseAllCells() {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.addAll(this.houseDataIdCells);
		cells.addAll(this.houseDataInfoCells);
		return cells;
	}

	public List<TableCell> getTenentInfoCells() {
		return this.tenentDataInfoCells;
	}

	public List<TableCell> getTenentAllCells() {
		List<TableCell> cells = new ArrayList<TableCell>();
		cells.addAll(this.tenentDataIdCells);
		cells.addAll(this.tenentDataInfoCells);
		return cells;
	}

	public HouseInfo getHouseInfo() throws Exception {
		HouseData houseData = new HouseData(getHouseAllCells());
		TenentData tenentData = new TenentData(getTenentAllCells());
		return new HouseInfo(houseData, tenentData);
	}

}
