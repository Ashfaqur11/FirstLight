package com.firstlight.homerent.core.ui.view.house;

import com.firstlight.homerent.core.model.HouseData;
import com.firstlight.homerent.core.model.TenentData;

public class HouseInfo {

	private final HouseData houseData;
	private final TenentData tenentData;

	public HouseInfo(HouseData house, TenentData tenent) {
		this.houseData = house;
		this.tenentData = tenent;
	}

	public HouseData getHouseData() {
		return houseData;
	}

	public TenentData getTenentData() {
		return tenentData;
	}

	public HouseInfo cloneData() throws Exception {
		HouseData houseData = new HouseData(this.houseData.getRowInfo());
		TenentData tenentData = new TenentData(this.tenentData.getRowInfo());
		return new HouseInfo(houseData, tenentData);
	}
}
