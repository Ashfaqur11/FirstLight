package com.firstlight.homerent.core.ui.view.house;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

import com.firstlight.homerent.core.model.TableCell;

public class DialogCellEditingSupport extends EditingSupport {

	private final CellEditor editor;
	private final TableViewer tableViewer;
	private final DialogHouseInfo dialog;
	private final DialogDataModel model;

	public DialogCellEditingSupport(ColumnViewer viewer, TableViewer tableViewer,
			DialogHouseInfo dialog, DialogDataModel model) {
		super(viewer);
		this.tableViewer = tableViewer;
		this.dialog = dialog;
		this.model = model;
		this.editor = new TextCellEditor(tableViewer.getTable());
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return this.editor;
	}

	@Override
	protected boolean canEdit(Object element) {
		if (element instanceof TableCell) {
			return true;
		}
		return false;
	}

	@Override
	protected Object getValue(Object element) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String value = cell.getCellValue();
			if (value != null) {
				return value;
			}
		}
		return "";
	}

	@Override
	protected void setValue(Object element, Object value) {
		if (element instanceof TableCell) {
			TableCell cell = (TableCell) element;
			String newValue = String.valueOf(value);
			if (newValue != null) {
				cell.setCellValue(newValue.isEmpty() ? null : newValue);
				tableViewer.update(element, null);
				dialog.validate();
			}
		}

	}
}
