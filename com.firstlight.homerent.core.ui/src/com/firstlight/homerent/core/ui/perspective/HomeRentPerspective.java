package com.firstlight.homerent.core.ui.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;
import org.eclipse.ui.console.IConsoleConstants;

import com.firstlight.homerent.core.ui.view.house.HousesView;
import com.firstlight.homerent.core.ui.views.ActionsView;
import com.firstlight.homerent.core.ui.views.RentView;

public class HomeRentPerspective implements IPerspectiveFactory {

	@Override
	public void createInitialLayout(IPageLayout layout) {
		createLayout(layout);
	}
	
	private void createLayout(IPageLayout layout) {
		layout.setEditorAreaVisible(false);
		String editorId = layout.getEditorArea();
		
		layout.addStandaloneView(ActionsView.ID, false, IPageLayout.LEFT, 0.06f, editorId);
		layout.getViewLayout(ActionsView.ID).setCloseable(false);
		layout.getViewLayout(ActionsView.ID).setMoveable(false);

		IFolderLayout folder = layout.createFolder("top", IPageLayout.TOP, 0.80f, editorId);
		folder.addView(HousesView.ID);
		folder.addPlaceholder(RentView.ID);
		
		layout.addView(IConsoleConstants.ID_CONSOLE_VIEW, IPageLayout.BOTTOM, 0.20f, editorId);

	}
}
