package com.firstlight.homerent.unit.test.core;

import org.junit.Assert;
import org.junit.Test;

import com.firstlight.homerent.core.sql.SqlBuilder;

public class SQLBuilderTest {

	@Test
	public void testSqlStringBuilders() {
		String inputTableName = "houses";
		String expectedOutput = "SELECT name FROM sqlite_master WHERE type='table' AND name='houses'";
		String output = SqlBuilder.findTable(inputTableName);
		Assert.assertEquals(expectedOutput, output);
	}
}
