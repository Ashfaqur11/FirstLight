package com.firstlight.homerent.common.utils;

import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class EclipseUtils {

	public static IWorkbenchPage getActivePage(){
		IWorkbench wb = PlatformUI.getWorkbench();
		IWorkbenchWindow window = wb.getActiveWorkbenchWindow();
		return window.getActivePage();
	}

	public static void openView(String id) throws PartInitException {
		IWorkbenchPage page = EclipseUtils.getActivePage();
		IViewPart view = page.findView(id);
		if (view == null || !page.isPartVisible(view)) {
			page.showView(id);
		}

	}

	public static IPath getWorkspaceRoot() {
		IWorkspace ws = ResourcesPlugin.getWorkspace();
		IWorkspaceRoot root = ws.getRoot();
		return root.getLocation();
	}

}
