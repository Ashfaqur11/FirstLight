package com.firstlight.homerent.common.logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.eclipse.core.runtime.ILog;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.console.ConsolePlugin;
import org.eclipse.ui.console.IConsole;
import org.eclipse.ui.console.IConsoleConstants;
import org.eclipse.ui.console.IConsoleManager;
import org.eclipse.ui.console.IConsoleView;
import org.eclipse.ui.console.MessageConsole;
import org.eclipse.ui.console.MessageConsoleStream;
import org.osgi.framework.Bundle;

import com.firstlight.homerent.common.Activator;
import com.firstlight.homerent.common.utils.EclipseUtils;

public class Logger {

	private static int logLevel = 0;

	private static final String CONSOLE_NAME = "Home Rent";

	public enum TYPE {
		DEBUG(0), INFO(1), WARN(2), ERROR(3);

		private final int level;

		TYPE(int level) {
			this.level = level;
		}

		public int getLevel() {
			return this.level;
		}

		public String getPrefix() {
			return getCurrentTime() + " [" + this.toString() + "] ";
		}
	}

	public static void debug(String msg) {
		logToConsole(TYPE.DEBUG, msg);
	}

	public static void info(String msg) {
		logToConsole(TYPE.INFO, msg);
	}

	public static void warn(String msg) {
		logToConsole(TYPE.WARN, msg);
	}

	public static void error(String msg) {
		logToConsole(TYPE.ERROR, msg);
		fileErrorLog(msg);
	}

	private static String getCurrentTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
		return sdf.format(new Date());
	}

	private static boolean isAllowed(int value) {
		return (value >= logLevel);
	}

	public static void error(String msg, Exception e) {
		logToConsole(TYPE.ERROR, msg + " Details: " + e.getMessage());
		fileErrorLog(msg, e);
	}

	private static void fileErrorLog(String message, Exception e) {
		String msg = TYPE.ERROR.getPrefix() + message;
		e.printStackTrace();
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		ILog platformLog = Platform.getLog(bundle);
		platformLog.log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg, e));
	}

	private static void fileErrorLog(String message) {
		String msg = TYPE.ERROR.getPrefix() + message;
		Bundle bundle = Platform.getBundle(Activator.PLUGIN_ID);
		ILog platformLog = Platform.getLog(bundle);
		platformLog.log(new Status(IStatus.ERROR, Activator.PLUGIN_ID, msg));
	}

	private static void logToConsole(TYPE type, String message) {
		if (isAllowed(type.getLevel())) {
			String msg = type.getPrefix() + message;
			try {
				MessageConsole console = findConsole();
				MessageConsoleStream out = console.newMessageStream();
				out.println(msg);
			} catch (Exception e) {
				fileErrorLog(TYPE.ERROR.getPrefix() + "Unable to show console view.", e);
			}
		}
	}

	/**
	 * Finds the console for the project. Creates a new
	 * console if not found. Opens the console if not
	 * visible
	 * 
	 * @return Console to show logs
	 * @throws PartInitException
	 */
	private static MessageConsole findConsole() throws PartInitException {

		MessageConsole messageConsole = null;

		ConsolePlugin consolePlugin = ConsolePlugin.getDefault();
		IConsoleManager consoleManager = consolePlugin.getConsoleManager();
		IConsole[] allConsoles = consoleManager.getConsoles();

		for (IConsole console : allConsoles) {
			if (console.getName().equals(CONSOLE_NAME)) {
				messageConsole = (MessageConsole) console;
			}
		}

		if (messageConsole == null) {
			messageConsole = new MessageConsole(CONSOLE_NAME, null);
			consoleManager.addConsoles(new IConsole[] { messageConsole });
		}

		IWorkbenchPage page = EclipseUtils.getActivePage();
		IViewPart consoleView = page.findView(IConsoleConstants.ID_CONSOLE_VIEW);
		if (consoleView == null && !page.isPartVisible(consoleView)) {
			IConsoleView view = (IConsoleView) page.showView(IConsoleConstants.ID_CONSOLE_VIEW);
			view.display(messageConsole);
		}

		return messageConsole;
	}

}
